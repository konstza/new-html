(function($) {

  var videoInitialized = false,
      currentVideoUrl = null,
      $video, video;
  function initializeVideo() {
    $video = $('<div class="video"></div>');
    video = document.createElement('video');
    video.setAttribute('controls', '');
    video.setAttribute('controlslist', 'nodownload');
    video.setAttribute('autoplay', '');

    var $videoOverlay = $('<div class="video__overlay"></div>'),
        $videoContent = $('<div class="video__content"></div>'),
        $videoClose = $('<div class="video__close"></div>');

    $videoOverlay.on('click', closeVideo);
    $videoClose.on('click', closeVideo);

    $videoContent.append(video);
    $video
      .append($videoOverlay)
      .append($videoContent)
      .append($videoClose)
      .appendTo('body');

    videoInitialized = true;
  }
  function closeVideo() {
    $video.removeClass('video--opened');
    video.pause();
  }
  function playVideo(url) {
    if (!url) return;

    !videoInitialized && initializeVideo();

    if (currentVideoUrl !== url) {
      currentVideoUrl = url;
      video.addEventListener('canplaythrough', function(e) {
        e.target.removeEventListener(e.type, arguments.callee);
        video.currentTime = 0;
      });
      video.setAttribute('src', url);
    } else {
      video.currentTime = 0;
      video.play();
    }

    requestAnimationFrame(function() {
      $video.addClass('video--opened');
    });
  }

  function initTop() {
    var $topVideo = $('.top-video');
    if (!$topVideo.length) return;

    $topVideo.on('click', function() {
      playVideo($topVideo.data('video'));
    });
  }

  function initSlider() {
    $('.review__items').slick({
      arrows: true,
      speed: 800
    });
  }

  function initAppearance() {
    appearance.defaultOptions({ offset: window.innerWidth < 1024 ? 50 : 100 });
    appearance.watch($('[data-appearance]'), {
      enterOnce: true
    });
  }

  function initSignPopup() {
    $(".login-btn").click(function() {
      $(".sign-in").addClass("active");
    });

    $(".sign-in__close").click(function() {
      $(".sign-in").removeClass("active");
    });
  }

  function initSolutionsExpand() {
    $("#solutions-expand").click(function() {

      if ($("#solutions").hasClass("expanded")) {
        $("#solutions").removeClass("expanded");
      } else {
        $("#solutions").addClass("expanded");
      }
    });
  }

  function initPeriodSelect() {
    $("#yearly-btn").click(function() {

      if ($("#yearly-btn").hasClass("chosen")) return;

      $(".solutions__period li").removeClass("chosen");
      $("#yearly-btn").addClass("chosen");
      $(".item__price span").removeClass("active");
      $(".price-yearly").addClass("active");

    });

    $("#monthly-btn").click(function() {

      if ($("#monthly-btn").hasClass("chosen")) return;

      $(".solutions__period li").removeClass("chosen");
      $("#monthly-btn").addClass("chosen");
      $(".item__price span").removeClass("active");
      $(".price-monthly").addClass("active");

    });
  }

  function initSlick() {

    var slider = document.querySelector(".solutions__items");

    if (slider) {

      var rect = slider.getBoundingClientRect();

      $('.solutions__items').slick({
        slidesToShow: 3,
        mobileFirst: true,
        prevArrow: "<button type='button' class='slick-prev'></button>",
        nextArrow: "<button type='button' class='slick-next'></button>",
        infinite: false,
        responsive: [
          {
            breakpoint: 1023,
            settings: "unslick"
          },
          {
            breakpoint: 680,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 0,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });

      if (rect.top <= window.innerHeight*0.65 || rect.top <= window.innerHeight*0.4 ) {
        $('.slick-arrow').addClass("visible");
      }

    }
  }

  function initArrows() {
    var rect, slider = document.querySelector(".solutions__items");
    if (slider) {
      $(window).on('scroll', function() {
        rect = slider.getBoundingClientRect();

        if (rect.top <= window.innerHeight*0.65) {
          $('.slick-arrow').addClass("visible");
        }

        if (rect.bottom < window.innerHeight*0.65 || rect.top > window.innerHeight*0.4) {
          $('.slick-arrow').removeClass("visible");
        }
      });
    }
  }

  function initResize() {
    $(window).resize(function() {
      if (window.innerWidth < 1024) {
        initSlick();
      }
    });
  }

  function initLang() {
    $(".header-lang").click(function() {
      if ($(".header-lang ul").hasClass("visible")) {
        $(".header-lang ul").removeClass("visible");
      } else {
        $(".header-lang ul").addClass("visible");

        requestAnimationFrame (function() {
          $(document).click(function() {
            console.log("CLICK");
            requestAnimationFrame(function() {
              $(".header-lang ul").removeClass("visible");
            });

            $(document).unbind('click');

          });
        });
      }
    });
  }

  $(function() {
    initTop();
    initSlider();
    initAppearance();
    initSignPopup();
    initSolutionsExpand();
    initPeriodSelect();
    initSlick();
    initArrows();
    initResize();
    initLang();
  });

})(jQuery);
