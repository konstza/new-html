const _ = require('lodash')
const gulp = require('gulp')
const jsbeautifier = require('gulp-jsbeautifier')
const nunjucksRender = require('gulp-nunjucks-render')
const rename = require('gulp-rename')
const replace = require('gulp-replace')
const util = require('gulp-util')

var defaultOptions = {
  src: null,
  dest: null,
  extname: 'html',
  jsbeautifier: {
     unformatted: [],
     indent_size: 2,
     indent_char: ' ',
     max_preserve_newlines: 0
  },
  minify: false
}

module.exports = (options) => {
  options = _.assign({}, defaultOptions, options)

  return gulp.src(options.src)
    .pipe(rename((path) => { path.extname = '.' + options.extname }))
    .pipe(nunjucksRender({ path: 'src/pages' }).on('error', util.log))
    .pipe(options.minify ? replace(/\s+/g, ' ') : jsbeautifier(defaultOptions.jsbeautifier))
    .pipe(gulp.dest("./dist"))
}