const _ = require('lodash')
const cheerio = require('gulp-cheerio')
const gulp = require('gulp')
const rename = require('gulp-rename')
const replace = require('gulp-replace')
const svgSprite = require('gulp-svg-sprite')
const svgmin = require('gulp-svgmin')

var defaultOptions = {
  src: null
}

module.exports = {
  sprite: (options) => {
    options = _.assign({}, defaultOptions, options)

    return gulp.src(options.src)
      .pipe(rename((path) => {
        path.basename = path.basename.toLowerCase()
      }))
      .pipe(cheerio({
        run: function ($) {
          $('style').remove();
        },
        parserOptions: {xmlMode: true}
      }))
      // cheerio plugin create unnecessary string '&gt;', so replace it.
      .pipe(replace('&gt;', '>'))
      .pipe(svgmin({
        plugins: [
          { removeTitle: true },
          { removeXMLNS: true },
          { removeAttrs: { attrs: ['id', 'class', 'fill'] } },
        ]
      }))
      .pipe(svgSprite({
        mode: {
          symbol: { sprite: "../sprite.svg" }
        },
        svg: { xmlDeclaration: false }
      }))
      .pipe(gulp.dest('./dist/images'))
  }
}